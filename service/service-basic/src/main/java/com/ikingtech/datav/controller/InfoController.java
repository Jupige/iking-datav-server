package com.ikingtech.datav.controller;

import com.ikingtech.datav.api.InfoApi;
import com.ikingtech.datav.model.entity.InfoDO;
import com.ikingtech.datav.model.request.InfoDTO;
import com.ikingtech.datav.model.request.InfoDeleteDTO;
import com.ikingtech.datav.model.request.InfoSearchDTO;
import com.ikingtech.datav.model.result.InfoVO;
import com.ikingtech.datav.service.InfoService;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author fucb
 */
@Slf4j
@ApiController(value = "/screen", name = "可视化大屏-基础信息表", description = "可视化大屏-基础信息表")
@RequiredArgsConstructor
public class InfoController implements InfoApi {

    private final InfoService infoService;


    @Override
    public R<List<InfoVO>> selectByPage(@RequestBody InfoSearchDTO param) {
        return R.ok(this.infoService.selectByPage(param));
    }

    @Override
    public R<InfoVO> getInfo(@RequestParam("id") String id, @RequestParam(value = "model", required = false) Boolean model) {
        if (Boolean.TRUE.equals(model)) {
            return R.ok(this.infoService.getInfoByTemplateId(id));
        }
        return R.ok(this.infoService.getInfo(id));
    }

    @Override
    public R<InfoVO> add(@RequestBody InfoDTO param) {
        InfoDO info = this.infoService.add(param);
        return R.ok(Tools.Bean.copy(info, InfoVO.class));
    }

    @Override
    public R<String> move(String id, String group) {
        this.infoService.lambdaUpdate().eq(InfoDO::getId, id).set(InfoDO::getGroupId, group).update();
        return R.ok(id);
    }

    @Override
    public R<String> edit(@RequestBody InfoDTO param) {
        this.infoService.edit(param);
        return R.ok();
    }
    @Override
    @PostRequest(value = "/share", summary = "发布分享大屏", description = "发布分享大屏")
    public R<String> share(@RequestBody InfoDTO param) {
        this.infoService.share(param);
        return R.ok();
    }

    @Override
    public R<String> delete(@RequestBody InfoDeleteDTO infoDeleteDTO) {
        this.infoService.delete(infoDeleteDTO.getIds());
        return R.ok();
    }

    @Override
    public R<InfoVO> template(@RequestBody InfoDTO param) {

        InfoDO infoDO = infoService.create(param);

        return R.ok(Tools.Bean.copy(infoDO, InfoVO.class));
    }

    @Override
    public R<String> rename(@RequestBody InfoDTO param) {
        InfoDO byId = this.infoService.getById(param.getId());
        byId.setName(param.getName());
        this.infoService.updateById(byId);
        return R.ok();
    }

    @Override
    public R<String> copy(@RequestBody InfoDTO param) {
        infoService.copy(param);
        return R.ok();
    }

}

