package com.ikingtech.datav.enums;

import lombok.RequiredArgsConstructor;

/**
 * created on 2024-03-06 09:44
 *
 * @author wub
 */

@RequiredArgsConstructor
public enum ComponentTypeEnum {

    /**
     * 收藏组件
     */
    COLLECTED("收藏组件"),
    /**
     * 模板组件
     */
    TEMPLATE("模板组件"),
    /**
     * 普通组件
     */
    NORMAL("普通组件");

    public final String description;
}
