package com.ikingtech.datav.model.constant;

import cn.hutool.core.text.CharSequenceUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.io.File;

/**
 * @author Listen
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FileConstant {

    /**
     * 业务标识
     */
    public static final String DATA_V = "dataV";
    /**
     * 文件基本前缀路径
     */
    public static String FILE_BASE_URL = "/datav/file";
    /**
     * 文件上传路径
     */
    public static String FILE_UPLOAD_PATH = FILE_BASE_URL + "/upload";
    /**
     * 资源包上传路径
     */
    public static String FILE_RESOURCE_PATH = FILE_BASE_URL + "/resource";
    /**
     * 场景文件上传路径
     */
    public static String FILE_SCENE_PATH = FILE_BASE_URL + "/scene";
    /**
     * 导出大屏文件路径
     */
    public static String EXTRACT_INFO = "info.json";
    /**
     * 构建资源路径
     */
    public static String BUILD_SOURCE = "deploy" + File.separator;
    /**
     * log图标路径
     */
    public static String LOGO_SOURCE = FILE_BASE_URL + "/logo.png";
    /**
     * 中国地图
     */
    public static String BASIC_MAP_SOURCE = FILE_BASE_URL + "/mapjson/100000.json";

    /**
     * 本地读取的资源路径
     */
    public static String LOCAL_SOURCE_FILE = "static/datav/file";
    public static String LOCAL_SOURCE_COM_PICTURE = LOCAL_SOURCE_FILE + "/com-picture";
    public static String LOCAL_SOURCE_IMAGE = LOCAL_SOURCE_FILE + "/image";
    public static String LOCAL_SOURCE_MAP_JSON = LOCAL_SOURCE_FILE + "/mapjson";
    public static String LOCAL_SOURCE_SYSTEM = LOCAL_SOURCE_FILE + "/system";
    public static String LOCAL_SOURCE_TEMPLATE = LOCAL_SOURCE_FILE + "/template";
    /**
     * 上传到minio的资源路径
     */
    public static String BUCKET_SOURCE_COM_PICTURE = FILE_BASE_URL + "/com-picture";
    public static String BUCKET_SOURCE_RESOURCE_IMAGE = FILE_BASE_URL + "/resource/image";
    public static String BUCKET_SOURCE_MAP_JSON = FILE_BASE_URL + "/mapjson";
    public static String BUCKET_SOURCE_SYSTEM = FILE_BASE_URL + "/system";
    public static String BUCKET_SOURCE_TEMPLATE = FILE_BASE_URL + "/template";
    /**
     * 部署文档本地路径
     */
    public static final String LOCAL_DEPLOY_PDF = "deploy/deploy.pdf";
    /**
     * 部署文档本地路径
     */
    public static final String LOCAL_NGINX_CONF = "deploy/nginx.conf";

    /**
     * 动态cdn地址
     */
    public static String FILE_CDN_URL = CharSequenceUtil.EMPTY;
}
