package com.ikingtech.datav.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.datav.model.entity.TemplateDO;

/**
 * @author fucb
 */
public interface TemplateMapper extends BaseMapper<TemplateDO> {

}

