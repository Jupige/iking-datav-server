package com.ikingtech.datav.api;

import com.ikingtech.datav.model.request.InfoDTO;
import com.ikingtech.datav.model.request.InfoDeleteDTO;
import com.ikingtech.datav.model.request.InfoSearchDTO;
import com.ikingtech.datav.model.result.InfoVO;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.web.annotation.GetRequest;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * created on 2024-06-05 10:44
 *
 * @author wub
 */
public interface InfoApi {

    @PostRequest(value = "/page", summary = "查询基础信息表-分页查询", description = "查询基础信息表-分页查询")
    R<List<InfoVO>> selectByPage(@RequestBody InfoSearchDTO param);

    @GetRequest(value = "/get/info", summary = "查询基础信息表详情", description = "查询基础信息表详情")
    R<InfoVO> getInfo(@RequestParam("id") String id, @RequestParam(value = "model", required = false) Boolean model);

    @PostRequest(value = "/add", summary = "新增基础信息表", description = "新增基础信息表")
    R<InfoVO> add(@RequestBody InfoDTO param);

    @GetRequest(value = "/move", summary = "移动分组", description = "移动分组")
    R<String> move(@RequestParam String id, @RequestParam String group);

    @PostRequest(value = "/update", summary = "编辑基础信息表", description = "编辑基础信息表")
    R<String> edit(@RequestBody InfoDTO param);

    @PostRequest(value = "/share", summary = "发布分享大屏", description = "发布分享大屏")
    R<String> share(@RequestBody InfoDTO param);

    @PostRequest(value = "/delete", summary = "删除基础信息表", description = "删除基础信息表")
    R<String> delete(@RequestBody InfoDeleteDTO infoDeleteDTO);

    @PostRequest(value = "/template", summary = "使用模板创建", description = "使用模板创建")
    R<InfoVO> template(@RequestBody InfoDTO param);

    @PostRequest(value = "/rename", summary = "重命名", description = "重命名")
    R<String> rename(@RequestBody InfoDTO param);

    @PostRequest(value = "/copy", summary = "复制", description = "复制")
    R<String> copy(@RequestBody InfoDTO param);

}
