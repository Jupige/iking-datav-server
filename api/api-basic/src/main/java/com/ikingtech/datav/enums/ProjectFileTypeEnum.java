package com.ikingtech.datav.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 文件类型
 *
 * @author Listen
 */
@AllArgsConstructor
@Getter
public enum ProjectFileTypeEnum {

    /**
     * 通用组件
     */
    COMMON("通用组件", "/common"),

    /**
     * 背景元素
     */
    BACKGROUND("背景元素", "/background"),

    /**
     * 背景元素
     */
    DECORATION("装饰元素", "/decoration"),

    /**
     * 图片动画
     */
    IMAGE("图片动画", "/image"),

    /**
     * 标注元素
     */
    MARK("标注元素", "/mark"),

    /**
     * 底座元素
     */
    BASE("底座元素", "/base"),

    /**
     * 顶部元素
     */
    TOP("顶部元素", "/top"),

;

    private final String value;

    /**
     * 文件服务器中地址路径
     */
    private final String path;
}
