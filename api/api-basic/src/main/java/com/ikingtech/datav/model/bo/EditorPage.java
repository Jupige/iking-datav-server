package com.ikingtech.datav.model.bo;

import lombok.Data;

import java.util.List;

/**
 * 编辑器子屏配置
 */
@Data
public class EditorPage {

    private String id;

    private String name;

    private String type;

    private Animation animation;

    private List<String> children;
}

@Data
class Animation {

    private Integer duration;

    private Boolean loop;

    private String type;

    private String css;

    private String img;

    private String video;

    private String lottie;
}
