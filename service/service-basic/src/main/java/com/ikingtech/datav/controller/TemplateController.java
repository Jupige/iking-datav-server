package com.ikingtech.datav.controller;

import cn.hutool.core.bean.BeanUtil;
import com.ikingtech.datav.api.TemplateApi;
import com.ikingtech.datav.model.entity.InfoDO;
import com.ikingtech.datav.model.request.TemplateDTO;
import com.ikingtech.datav.model.request.TemplatePublishDTO;
import com.ikingtech.datav.model.request.TemplateSearchDTO;
import com.ikingtech.datav.model.result.TemplateVO;
import com.ikingtech.datav.service.InfoService;
import com.ikingtech.datav.service.TemplateService;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author fucb
 */
@ApiController(value = "/template", name = "可视化大屏-模板管理", description = "可视化大屏-模板管理")
@RequiredArgsConstructor
public class TemplateController implements TemplateApi {

    private final TemplateService templateService;

    private final InfoService infoService;

    @Override
    public R<List<TemplateVO>> selectByPage(@RequestBody(required = false) TemplateSearchDTO param) {
        if (param == null) {
            param = new TemplateSearchDTO();
        }
        if (param.getRows() <= 0) {
            param.setRows(15);
        }
        return R.ok(this.templateService.selectByPage(param).convert(entity -> BeanUtil.copyProperties(entity,TemplateVO.class)));
    }

    @Override
    public R<TemplateVO> getInfo(@RequestParam("id") String id) {
        return R.ok(this.templateService.getInfo(id));
    }

    @Override
    public R<String> add(@RequestBody TemplatePublishDTO param) {
        this.templateService.add(param.getTemplate());
        return R.ok();
    }

    @Override
    public R<String> turn(@RequestBody TemplatePublishDTO param) {
        InfoDO byId = infoService.getById(param.getScreen().getId());
        templateService.turn(param, byId);
        return R.ok();
    }

    @Override
    public R<String> edit(@RequestBody TemplateDTO param) {
        this.templateService.edit(param);
        return R.ok();
    }

    @Override
    public R<String> delete(@RequestParam("id") String id) {
        this.templateService.delete(id);
        return R.ok();
    }

}

