package com.ikingtech.datav.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author fucb
 */

@RequiredArgsConstructor
public enum ProjectFileExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 项目文件不存在
     */
    PROJECT_FILE_NOT_FOUND("projectFileNotFound"),

    /**
     * 文件上传失败
     */
    FILE_UPLOAD_FAILED("fileUploadFailed"),

    /**
     * 文件下载失败
     */
    FILE_DOWNLOAD_FAILED("fileDownloadFailed"),

    /**
     * 文件服务器连接失败
     */
    FILE_SERVICE_ERROR("文件服务器连接失败,请检查配置信息"),

    ;

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "iking-framework-maintenace-datav";
    }
}
