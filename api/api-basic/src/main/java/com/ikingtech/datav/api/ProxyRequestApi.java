package com.ikingtech.datav.api;

import com.ikingtech.datav.model.request.ProxyRequestDTO;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * created on 2024-06-05 10:54
 *
 * @author wub
 */

public interface ProxyRequestApi {
    @PostRequest(value = "proxy", summary = "代理请求", description = "服务器代理请求")
    Object proxyRequest(@RequestBody @Validated ProxyRequestDTO proxyRequestDTO, HttpServletRequest request);
}
