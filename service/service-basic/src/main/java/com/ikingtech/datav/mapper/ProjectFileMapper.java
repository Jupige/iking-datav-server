package com.ikingtech.datav.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.datav.model.entity.ProjectFileDO;

/**
 * @author fucb
 */
public interface ProjectFileMapper extends BaseMapper<ProjectFileDO> {

}

