package com.ikingtech.datav.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.datav.model.entity.TemplateComponentDO;

public interface TemplateComponentMapper extends BaseMapper<TemplateComponentDO> {
}