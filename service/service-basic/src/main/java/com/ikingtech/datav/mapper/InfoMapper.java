package com.ikingtech.datav.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.datav.model.entity.InfoDO;

/**
 * @author fucb
 */
public interface InfoMapper extends BaseMapper<InfoDO> {

}

