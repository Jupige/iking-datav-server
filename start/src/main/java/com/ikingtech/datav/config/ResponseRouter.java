package com.ikingtech.datav.config;

import cn.hutool.core.util.StrUtil;
import com.ikingtech.datav.model.constant.FileConstant;

public class ResponseRouter {

    private static final String DATA_V_PREFIX = "/datav/";

    public String filter(String data, String path) {
        boolean result = un().contains(path);
        if (result) {
            return data;
        }

        if (StrUtil.isNotBlank(data) && data.contains(DATA_V_PREFIX)) {
            data = StrUtil.replace(data, DATA_V_PREFIX, FileConstant.FILE_CDN_URL + DATA_V_PREFIX);
        }

        return data;
    }

    public static String un() {
        return "";
    }
}
