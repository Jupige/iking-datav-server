package com.ikingtech.datav.model.request;

import com.ikingtech.datav.enums.MethodEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

/**
 * created on 2024-03-20 15:37
 *
 * @author wub
 */

@Data
public class ProxyRequestDTO {

    @NotBlank(message = "代理接口必必传")
    @Schema(name = "proxyApi", description = "代理接口")
    private String proxyApi;

    @Schema(name = "proxyMethod", description = "代理接口方法")
    private MethodEnum proxyMethod;

}
