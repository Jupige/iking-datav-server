package com.ikingtech.datav.enums;

/**
 * created on 2024-06-05 10:56
 *
 * @author wub
 */

public enum MethodEnum {
    GET, POST, HEAD, OPTIONS, PUT, DELETE, TRACE, CONNECT, PATCH
}
