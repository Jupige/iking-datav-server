package com.ikingtech.datav.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.datav.mapper.TemplateComponentMapper;
import com.ikingtech.datav.model.entity.TemplateComponentDO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TemplateComponentService extends ServiceImpl<TemplateComponentMapper, TemplateComponentDO> {

    public List<TemplateComponentDO> getComponent(String templateId) {
        return lambdaQuery().eq(TemplateComponentDO::getTemplateId, templateId).list();
    }

    public List<TemplateComponentDO> getComponent(List<String> ids) {
        return lambdaQuery().in(TemplateComponentDO::getId, ids).list();
    }
}
