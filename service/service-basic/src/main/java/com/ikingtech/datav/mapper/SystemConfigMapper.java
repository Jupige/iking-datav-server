package com.ikingtech.datav.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.datav.model.entity.SystemConfigDO;

/**
 * created on 2024-03-22 14:42
 *
 * @author wub
 */

public interface SystemConfigMapper extends BaseMapper<SystemConfigDO> {
}
