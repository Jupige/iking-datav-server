package com.ikingtech.datav.model.request;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author fucb
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Schema(name = "InfoSearchDTO对象", description = "基础信息表对象")
public class InfoSearchDTO extends PageParam {


    /**
     * ID
     */
    @Schema(name = "id", description = "ID")
    private String id;

    /**
     * 大屏ID
     */
    @Schema(name = "screenId", description = "大屏ID")
    private String screenId;

    /**
     * 名称
     */
    @Schema(name = "name", description = "名称")
    private String name;

    /**
     * 组ID
     */
    @Schema(name = "groupId", description = "组ID")
    private String groupId;

    /**
     * 模板ID
     */
    @Schema(name = "templateId", description = "模板ID")
    private String templateId;

    /**
     * 分享地址
     */
    @Schema(name = "shareUrl", description = "分享地址")
    private String shareUrl;


}

