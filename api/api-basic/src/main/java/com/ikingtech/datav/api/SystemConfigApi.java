package com.ikingtech.datav.api;

import com.ikingtech.datav.model.request.SystemConfigDTO;
import com.ikingtech.datav.model.request.SystemConfigSearchDTO;
import com.ikingtech.datav.model.result.SystemConfigVO;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.web.annotation.DeleteRequest;
import com.ikingtech.framework.sdk.web.annotation.GetRequest;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * created on 2024-06-05 10:59
 *
 * @author wub
 */

public interface SystemConfigApi {
    @PostRequest(value = "/select/page", summary = "查询系统配置表-分页查询", description = "查询系统配置表-分页查询")
    R<List<SystemConfigVO>> selectByPage(@RequestBody SystemConfigSearchDTO param);

    @GetRequest(value = "/get/info", summary = "查询系统配置表详情", description = "查询系统配置表详情")
    R<SystemConfigVO> getInfo(@RequestParam("id") String id);

    @PostRequest(value = "/add", summary = "新增系统配置表", description = "新增系统配置表")
    R<String> add(@RequestBody SystemConfigDTO param);

    @PostRequest(value = "/edit", summary = "编辑系统配置表", description = "编辑系统配置表")
    R<String> edit(@RequestBody SystemConfigDTO param);

    @DeleteRequest(value = "/delete", summary = "删除系统配置表", description = "删除系统配置表")
    R<String> delete(@RequestParam("id") String id);

    @GetRequest(value = "/getFileUrl", summary = "获取dataV配置的文件服务器地址", description = "获取dataV配置的文件服务器地址")
    R<String> getFileUrl();

    @GetRequest(value = "/by/type", summary = "根据type查询配置", description = "根据type查询配置")
    R<SystemConfigVO> byType(@RequestParam("type") String type);
}
