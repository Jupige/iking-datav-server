package com.ikingtech.datav.api;

import com.ikingtech.datav.model.request.TemplateDTO;
import com.ikingtech.datav.model.request.TemplatePublishDTO;
import com.ikingtech.datav.model.request.TemplateSearchDTO;
import com.ikingtech.datav.model.result.TemplateVO;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.web.annotation.DeleteRequest;
import com.ikingtech.framework.sdk.web.annotation.GetRequest;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * created on 2024-06-05 11:01
 *
 * @author wub
 */

public interface TemplateApi {

    @PostRequest(value = "/page", summary = "查询-分页查询", description = "查询-分页查询")
    R<List<TemplateVO>> selectByPage(@RequestBody(required = false) TemplateSearchDTO param);

    @GetRequest(value = "/get/info", summary = "查询详情", description = "查询详情")
    R<TemplateVO> getInfo(@RequestParam("id") String id);

    @PostRequest(value = "/add", summary = "新增模板", description = "新增模板")
    R<String> add(@RequestBody TemplatePublishDTO param);

    @PostRequest(value = "/turn", summary = "大屏转为模板（发布）", description = "大屏转为模板（发布）")
    R<String> turn(@RequestBody TemplatePublishDTO param);

    @PostRequest(value = "/edit", summary = "编辑模板", description = "编辑模板")
    R<String> edit(@RequestBody TemplateDTO param);

    @DeleteRequest(value = "/delete", summary = "删除", description = "删除")
    R<String> delete(@RequestParam("id") String id);

}
