package com.ikingtech.datav.model.request;

import lombok.Data;

@Data
public class TemplatePublishDTO {

    /**
     * 屏幕信息
     */
    private InfoDTO screen;

    /**
     * 模板信息
     */
    private TemplateDTO template;
}
