package com.ikingtech.datav.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.datav.model.entity.FilterDO;

/**
 * @author fucb
 */
public interface FilterMapper extends BaseMapper<FilterDO> {

}

