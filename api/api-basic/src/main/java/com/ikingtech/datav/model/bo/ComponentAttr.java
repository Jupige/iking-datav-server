package com.ikingtech.datav.model.bo;

import lombok.Data;

@Data
public class ComponentAttr {

    private Boolean apply3d;

    private Integer perspective;

    private PerspectiveOrigin  perspectiveOrigin;

    private Integer x;

    private Integer y;

    private Integer w;

    private Integer h;

    private Integer deg;

    private Integer opacity;

    private Boolean filpV;

    private Boolean filpH;
}

@Data
class PerspectiveOrigin {
    private String x;
    private String y;
}
