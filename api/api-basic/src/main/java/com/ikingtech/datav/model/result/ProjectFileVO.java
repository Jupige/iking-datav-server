package com.ikingtech.datav.model.result;

import com.ikingtech.datav.enums.ProjectFileTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * @author fucb
 */

@Data
@Schema(name = "ProjectFileVO对象", description = "对象")
public class ProjectFileVO implements Serializable {


    /**
     * ID
     */
    @Schema(name = "id", description = "ID")
    private String id;

    /**
     * 用户ID
     */
    @Schema(name = "userId", description = "用户ID")
    private String userId;

    /**
     * 用户名称
     */
    @Schema(name = "userName", description = "用户名称")
    private String userName;

    /**
     * 文件名称
     */
    @Schema(name = "fileName", description = "文件名称")
    private String fileName;

    /**
     * 项目ID
     */
    @Schema(name = "projectId", description = "项目ID")
    private String projectId;

    /**
     * 文件类型
     */
    @Schema(name = "type", description = "文件类型")
    private ProjectFileTypeEnum type;

    /**
     * 文件路径
     */
    @Schema(name = "path", description = "文件路径")
    private String path;

    /**
     * 文件混淆名称
     */
    @Schema(name = "mixFileName", description = "文件混淆名称")
    private String mixFileName;

    /**
     * 文件大小
     */
    @Schema(name = "size", description = "文件大小")
    private Long size;

}

