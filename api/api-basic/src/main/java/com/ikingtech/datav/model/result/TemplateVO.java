package com.ikingtech.datav.model.result;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author fucb
 */

@Data
@Schema(name = "TemplateVO对象", description = "对象")
public class TemplateVO implements Serializable {

    @Schema(name = "id", description = "ID")
    private String id;

    @Schema(name = "description", description = "描述")
    private String description;

    @Schema(name = "config", description = "配置")
    private String config;

    @Schema(name = "name", description = "名称")
    private String name;

    @Schema(name = "width", description = "宽度")
    private Integer width;

    @Schema(name = "height", description = "高度")
    private Integer height;

    @Schema(name = "snapshot", description = "快照")
    private String snapshot;

    @Schema(name = "coms", description = "组件")
    private String coms;

    @Schema(name = "isSystem", description = "描述")
    private Boolean isSystem;

    @Schema(name = "userId", description = "用户ID")
    private String userId;

    @Schema(name = "createBy", description = "创建人")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "createTime", description = "创建时间")
    private LocalDateTime createTime;

    @Schema(name = "updateBy", description = "修改人")
    private String updateBy;

    @Schema(name = "updateName", description = "修改人姓名")
    private String updateName;

    @Schema(name = "updateTime", description = "更新时间")
    private LocalDateTime updateTime;

}

