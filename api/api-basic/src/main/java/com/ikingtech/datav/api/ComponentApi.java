package com.ikingtech.datav.api;

import com.ikingtech.datav.model.request.ComponentDTO;
import com.ikingtech.datav.model.request.ComponentSearchDTO;
import com.ikingtech.datav.model.result.ComponentVO;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.web.annotation.GetRequest;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * created on 2024-06-04 18:40
 *
 * @author wub
 */

public interface ComponentApi {

    /**
     * 查询-个人收藏和公共收藏的组件
     *
     * @return 查询结果
     */
    @GetRequest(value = "/list", summary = "查询-个人收藏和公共收藏的组件", description = "查询-个人收藏和公共收藏的组件")
    R<List<ComponentVO>> adminComComponentList();

    /**
     * 分页查询
     *
     * @param param 筛选条件
     * @return 查询结果
     */
    @PostRequest(value = "/select/page", summary = "查询-分页查询", description = "查询-分页查询(NORMAL-普通组件,COLLECTED-收藏组件,TEMPLATE-模板组件)")
    R<List<ComponentVO>> selectByPage(@RequestBody ComponentSearchDTO param);

    /**
     * 查询详情
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetRequest(value = "/get/info", summary = "查询详情", description = "查询详情")
    R<ComponentVO> getInfo(@RequestParam("id") String id);

    /**
     * 新增
     *
     * @param param 实体
     * @return 新增结果
     */
    @PostRequest(value = "/add", summary = "新增", description = "新增(NORMAL-普通组件,COLLECTED-收藏组件,TEMPLATE-模板组件)")
    R<?> add(@RequestBody ComponentDTO param);

    /**
     * 批量新增
     *
     * @param param 实体
     * @return 新增结果
     */
    @PostRequest(value = "/adds", summary = "新增列表", description = "新增(NORMAL-普通组件,COLLECTED-收藏组件,TEMPLATE-模板组件)")
    R<?> add(@RequestBody List<ComponentDTO> param);

    /**
     * 编辑
     *
     * @param param 实体
     * @return 编辑结果
     */
    @PostRequest(value = "/edit", summary = "编辑", description = "编辑")
    R<String> edit(@RequestBody ComponentDTO param);

    /**
     * 删除
     *
     * @param list 主键
     * @return 删除是否成功
     */
    @PostRequest(value = "/del", summary = "删除", description = "删除")
    R<String> delete(@RequestBody List<String> list);

    /**
     * 获取组件列表
     *
     * @param screenId 大屏ID
     * @param model    是否子屏
     * @return 组件列表
     */
    @GetRequest(value = "/coms", summary = "查询-根据屏幕id获取组件列表", description = "根据屏幕id获取组件列表")
    R<List<ComponentVO>> comS(@RequestParam String screenId, @RequestParam(value = "model", required = false) Boolean model);

    /**
     * 更新大屏
     *
     * @param componentDTOList 组件列表
     * @return 是否成功
     */
    @PostRequest(value = "/updates", summary = "查询-保存大屏", description = "查询-保存大屏updates")
    R<?> updates(@RequestBody List<ComponentDTO> componentDTOList);

    /**
     * 删除组件
     *
     * @param param 组件列表
     * @return 删除结果
     */
    @PostRequest(value = "/delete/by/screen")
    R<?> deleteByScreen(@RequestBody ComponentDTO param);

    /**
     * 组件成组
     *
     * @param param 组件
     * @return 保存结果
     */
    @PostRequest(value = "/collection")
    R<?> collection(@RequestBody ComponentDTO param);

    /**
     * 删除列表
     *
     * @param dto 请求结果
     * @return 删除结果
     */
    @PostRequest(value = "/delete/collection")
    R<?> deleteCollection(@RequestBody ComponentDTO dto);
}
