package com.ikingtech.datav.model.request;

import lombok.Data;

import java.util.List;

/**
 * created on 2024-03-08 11:48
 *
 * @author wub
 */

@Data
public class BuildDeploymentDTO {

    private List<String> id;

    private String projectId;
}
