package com.ikingtech.datav.model.request;

import lombok.Data;

@Data
public class InfoDeleteDTO {

    private String ids;
}
