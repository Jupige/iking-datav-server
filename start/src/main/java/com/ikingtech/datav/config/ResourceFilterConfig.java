package com.ikingtech.datav.config;

import com.ikingtech.framework.sdk.context.constant.CommonConstants;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * created on 2024-03-05 11:16
 *
 * @author wub
 */

@Configuration
public class ResourceFilterConfig implements WebMvcConfigurer {

    @Bean
    public FilterRegistrationBean<ResourceFilter> myCustomFilterRegistration() {
        FilterRegistrationBean<ResourceFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new ResourceFilter());
        // 设置过滤器适用的URL模式
        registrationBean.addUrlPatterns("/*");
        // 设置过滤器的名字
        registrationBean.setName("resourceFilter");
        // 设置过滤器的执行顺序，数字越小越先执行
        registrationBean.setOrder(CommonConstants.SECURITY_FILTER_ORDER + 1);
        return registrationBean;
    }
}
