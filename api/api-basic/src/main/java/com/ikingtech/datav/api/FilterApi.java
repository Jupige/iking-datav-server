package com.ikingtech.datav.api;

import com.ikingtech.datav.model.request.FilterDTO;
import com.ikingtech.datav.model.request.FilterSearchDTO;
import com.ikingtech.datav.model.result.FilterVO;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.web.annotation.GetRequest;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * created on 2024-06-05 10:38
 *
 * @author wub
 */

public interface FilterApi {

    @PostRequest(value = "/select/page", summary = "查询-分页查询", description = "查询-分页查询(COMMON-普通过滤器,TEMPLATE-模板过滤器)")
    R<List<FilterVO>> selectByPage(@RequestBody FilterSearchDTO param);

    @GetRequest(value = "/get/info", summary = "查询详情", description = "查询详情")
    R<FilterVO> getInfo(@RequestParam("id") Integer id);

    @PostRequest(value = "/add", summary = "新增", description = "新增(COMMON-普通过滤器,TEMPLATE-模板过滤器)")
    R<String> add(@RequestBody FilterDTO param);

    @PostRequest(value = "/update", summary = "编辑", description = "编辑")
    R<String> edit(@RequestBody FilterDTO param);

    @GetRequest(value = "/delete", summary = "删除", description = "删除")
    R<String> delete(@RequestParam String id);

    @PostRequest(value = "/lists", summary = "查询-查询大屏的所有过滤器", description = "查询-分页查询(COMMON-普通过滤器,TEMPLATE-模板过滤器)")
    R<List<FilterVO>> filterLists(@RequestBody FilterSearchDTO param);
}
