package com.ikingtech.datav.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.datav.model.entity.ComponentDO;

/**
 * 组件表(ComponentDO)数据库访问层
 *
 * @author fucb
 * @since 2024-02-22 09:28:38
 */
public interface ComponentMapper extends BaseMapper<ComponentDO> {

}

