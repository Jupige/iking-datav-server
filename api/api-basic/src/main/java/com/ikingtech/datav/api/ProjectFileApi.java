package com.ikingtech.datav.api;

import com.ikingtech.datav.enums.ProjectFileTypeEnum;
import com.ikingtech.datav.model.request.ProjectFileDTO;
import com.ikingtech.datav.model.request.ProjectFileDeleteDTO;
import com.ikingtech.datav.model.request.ProjectFileSearchDTO;
import com.ikingtech.datav.model.result.ProjectFileVO;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.dict.model.DictItemDTO;
import com.ikingtech.framework.sdk.web.annotation.GetRequest;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * created on 2024-06-05 10:48
 *
 * @author wub
 */

public interface ProjectFileApi {

    @PostRequest(value = "/import/resource", summary = "导入资源包", description = "导入资源包")
    R<String> importResource(@RequestPart MultipartFile file) throws IOException;

    @GetRequest(value = "/type", summary = "获取类型", description = "获取类型")
    R<List<DictItemDTO>> type();

    @PostRequest(value = "upload", summary = "文件上传", description = "文件上传")
    R<ProjectFileDTO> upload(@RequestPart MultipartFile file,
                             @RequestParam(required = false) ProjectFileTypeEnum type,
                             @RequestParam(required = false) String projectId);

    @PostRequest(value = "/select/page", summary = "查询-分页查询", description = "查询-分页查询")
    R<List<ProjectFileVO>> selectByPage(@RequestBody ProjectFileSearchDTO param);

    @GetRequest(value = "/get/info", summary = "查询详情", description = "查询详情")
    R<ProjectFileVO> getInfo(@RequestParam("id") String id);

    @PostRequest(value = "/file/deletes", summary = "删除", description = "删除")
    R<String> delete(@RequestBody ProjectFileDeleteDTO projectFileDeleteDTO);

    @GetRequest(value = "/existsObject", summary = "文件是否存在", description = "文件是否存在")
    R<Boolean> existsObject(@RequestParam("path") String path);

}
