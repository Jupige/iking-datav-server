package com.ikingtech.datav.model.result;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 组件表(ComponentDO)对象
 *
 * @author fucb
 * @since 2024-02-22 09:28:47
 */

@Data
@Accessors(chain = true)
@Schema(name = "ComponentVO对象", description = "对象")
public class ComponentVO implements Serializable {

    @Schema(name = "id", description = "id")
    private String id;

    @Schema(name = "screenId", description = "大屏ID")
    private String screenId;

    @Schema(name = "sortOrder", description = "排序序号")
    private Integer sortOrder;

    @Schema(name = "type", description = "类型")
    private String type;

    @Schema(name = "alias", description = "别名")
    private String alias;

    @Schema(name = "name", description = "名称")
    private String name;

    @Schema(name = "children", description = "子组件")
    private List<Object> children;

    @Schema(name = "locked", description = "是否锁定")
    private Boolean locked;

    @Schema(name = "parentId", description = "父级ID")
    private String parentId;

    @Schema(name = "hided", description = "是否隐藏")
    private Boolean hided;

    @Schema(name = "icon", description = "图标")
    private String icon;

    @Schema(name = "img", description = "图片")
    private String img;

    @Schema(name = "attr", description = "属性")
    private Object attr;

    @Schema(name = "config", description = "配置")
    private Object config;

    @Schema(name = "apis", description = "API配置")
    private Object apis;

    @Schema(name = "apiData", description = "API数据")
    private Object apiData;

    @Schema(name = "animate", description = "入场动画")
    private Object animate;

    @Schema(name = "bgImg", description = "背景图")
    private Object bgImg;

    @Schema(name = "scaling", description = "组内配置")
    private Object scaling;

    @Schema(name = "events", description = "事件")
    private Object events;

    @Schema(name = "selected", description = "是否选中")
    private Boolean selected;

    @Schema(name = "hovered", description = "是否悬停")
    private Boolean hovered;

    @Schema(name = "bgAnimation", description = "背景动画")
    private String bgAnimation;

    @Schema(name = "dialog", description = "弹窗配置")
    private Object dialog;

    @Schema(name = "isDialog", description = "是否为弹窗")
    private Boolean isDialog;

    @Schema(name = "version", description = "版本")
    private Integer version;

    @Schema(name = "isPublic", description = "是否为公共收藏")
    private Integer isPublic;

    @Schema(name = "projectId", description = "项目ID")
    private String projectId;

    @Schema(name = "templateId", description = "模板ID")
    private String templateId;

    @Schema(name = "disActions", description = "响应事件")
    private Object disActions;

    @Schema(name = "componentType", description = "组件类型（普通组件，收藏组件，模板组件）")
    private String componentType;

    @Schema(name = "createTime", description = "创建时间")
    private LocalDateTime createTime;

    @Schema(name = "updateBy", description = "修改人")
    private String updateBy;

    @Schema(name = "createBy", description = "创建人")
    private String createBy;

    @Schema(name = "createName", description = "创建人名称")
    private String createName;

    @Schema(name = "updateName", description = "修改人名称")
    private String updateName;

    @Schema(name = "updateTime", description = "修改时间")
    private LocalDateTime updateTime;

}
