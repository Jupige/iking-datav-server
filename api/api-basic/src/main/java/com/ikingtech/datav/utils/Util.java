package com.ikingtech.datav.utils;


import cn.hutool.core.lang.TypeReference;
import cn.hutool.json.JSONUtil;

public class Util {

    public static class Json {
        public static <T> T toBean(String jsonStr) {
            if (JSONUtil.isTypeJSON(jsonStr)) {
                return JSONUtil.toBean(jsonStr, new TypeReference<>() {
                }, true);
            }
            return null;
        }
    }

}
