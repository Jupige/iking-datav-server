package com.ikingtech.datav.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * created on 2024-03-18 15:55
 *
 * @author wub
 */

@AllArgsConstructor
@Getter
public enum MediaFileTypeEnum {

    /**
     * 图片
     */
    IMAGE(Arrays.asList("jpg", "jpeg", "bmp", "png", "tiff", "raw", "gif", "swf", "fla", "webp", "xbm", "tif", "jff", "ico", "svg", "svgz", "pjp", "apng", "pipeg", "avif")),
    /**
     * 视频
     */
    VIDEO(Arrays.asList("mp4", "avi", "rmvb", "flv", "mov", "mkv", "wmv", "3gp", "webm", "mpeg", "vob", "mpg", "ogm", "ogv", "asx", "m4v")),
    /**
     * 视频
     */
    MODEL(Arrays.asList("gltf")),
    /**
     * 其他
     */
    OTHER(new ArrayList<>());

    private final List<String> extensions;

    public static List<String> getMediaType() {
        List<String> extensions = new ArrayList<>();
        extensions.addAll(IMAGE.getExtensions());
        extensions.addAll(VIDEO.getExtensions());
        extensions.addAll(MODEL.getExtensions());
        return extensions;
    }

    public static List<String> getExtensionsByType(String type) {
        return switch (type) {
            case "IMAGE" -> IMAGE.getExtensions();
            case "VIDEO" -> VIDEO.getExtensions();
            case "MODEL" -> MODEL.getExtensions();
            default -> OTHER.getExtensions();
        };
    }


}
